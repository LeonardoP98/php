<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>variabili</title>
</head>
<body>
    <?php
//le variabili hanno sempre simbolo del dollaro!!!
//questa � un istruzione che associa alla variabile "a" il valore "1". in php � un intero ..
$a = 2;
//questa � un istruzione che associa alla variabile "b" una stringa 
$b = "stringa lunga quanto voglio";
//la variabile "c" rappresenta falso. le altre variabili sono vere
$c = true;

//le variabili le posso chiamare come voglio .. consigliabile con underscore o con lettere maiuscole .. no spazi
$un_numero_a_caso = "io sono" . "pippo" . "tu sei pluto";

//cosa vuol dire concatenare un numero con una stringa? cosa fa l'interprete php quando gli dico concatenare un intero con una stringa? lui fa stringa 2 concatenata a pippo
$unNumeroAcaso = 2 . "pippo";
//per stampare una variabile .. da notare i due modi per riuscire a stampare le virgolette dentro alle stringhe
echo 'stampo il valore della variabile "a" :' . $a . "<br>" . "e il valore della variabile  \" b \" : ".$b ;
echo "<br>";
echo $unNumeroAcaso;

//ricardarsi che si CONCATENA solo con il punto. Con la virgola si aggiungono le cose da stampare con echo
echo "<br>" ,'attacco con virgola la variabile "a" :' , $a , "<br>" , "alla variabile  \" b \" : ".$b ;

//se cambio la variabile $a .. cambio il numero con cui dico ... quante volte BASTAAAA!!!
echo "<br>" . "io dico BASTA $a volte!!! � l'ora giusta per andare a casa!!!";
//ma tra una virgoletta la variabile viene letta come stringa!!! quindi devo concatenare .. o meglio usare le due virgolette.. anche perch� un apostrofo pu� incasinare
echo "<br>" . 'io dico BASTA $a volte!!! � l\'ora giusta per andare a casa!!!';
echo "<br>" . 'io dico BASTA' . $a . 'volte!!!';

echo "<br><br><hr><br><br>";
//se riscrivo le variabile da questo punto in poi variano ... infatti sono VARIABILI
$a=1;
$b=2;
echo $a+$b;

echo "a=" . $a . "b=" . $b . "<br>la somma di a+b=" . $a+$b;
echo "<br>";
echo "<br> $a+$b=". $a+$b;
echo "<br>";
//bisogna mettere le operazione tra parentesi .. se non la macchina interpreta tutto quello che c'� prima del + in modo numerico (stringhe comprese) e le somma alla variabile $b .. quindi:

echo ("2"+2);
echo "<br>";
//devo usare parentesi
echo "<br> $a+$b=". ($a+$b);

echo "<br>";
echo "<hr>";

echo "<br> $a+$b=". ($a+$b);
echo "<br> $a-$b=". ($a-$b);
echo "<br> $a*$b=". ($a*$b);
echo "<br> $a/$b=". ($a/$b);
//questo � un modulo .. restituisce il resto della divisione quindi mi fa capire se il numero � pari o dispari
echo "<br> $b%$b=". ($b%$b);
echo "<br> $a%$b=". ($a%$b);

echo "<br>";
echo "<hr>";

//provo gli operatori uguale o diverso. la risposta sar� 0 se � FALSO e 1 se � VERO (numeri con cui poter fare a sua volta altre operazioni). quindi se � falso non stamper� nulla se � vero stamper� 1

echo "$a==$b?" .($a==$b);
echo "$a==$b?" .($a!=$b);

echo "<br>";
echo "<hr>";

//assegnazioni .. $c diventa uguale a $2 .... $b diventa uguale a c .. ecc...

echo $a =$b =$c = 2;
echo "<br>";

echo $a;
echo "<br>";
echo $b;
echo "<br>";
echo $c;

echo "<br>";
echo "<hr>";

$a=5;
echo " $a+1" . "il risultato successivo �: " . ($a+1);

echo "<br>";
echo "<hr>";

//$a=$a+1;
echo $a=$a+1;

echo "<br>";
echo "<hr>";

echo "qualcosa";
$a++;
echo $a;


echo "<br>";
echo "<hr>";

echo "qualcosa";
++$a;
echo $a;



echo "<br>";
echo "<hr>";

$a=1;
// $a= $a +5;
// si trover� tante volte .. � come scrivere ...
$a +=5;



echo "<br>";
echo "<hr>";


//operatore ternario -- condizione???

$voto=30;
echo "voto=$voto<br>";

echo ($voto>18)? "promosso": "bocciato";
echo ($voto==30)? " col massimo dei voti": "";



echo "<br>";
echo "<hr>";


//se il voto � trenta devo aggiungere promosso col massimo dei voti


echo ($voto>18)? "promosso" . ( ($voto==30)? " col massimo dei voti": ""): "bocciato";

echo "<br>";
echo "<hr>";



echo ($voto==30)?"promosso con massimo":(($voto>=18)?"promosso":"bocciato");

echo "<br>";
echo "<hr>";

//la condizione che l if valuta va sempre tra parentesi tonda .. nella graffa ci sono le conseguenze .. conviene metterla sempre anche se ce n'� solo una
//dentro le graffe c'� quello che si verifica SOLO SE LA CONDIZIONE che posto tra parentesi tonde.. � VERA
//fuori dalle graffe c'� quella FALSA

$voto=27;

if($voto>=18){
    echo "promosso<br>";
    echo "eseguita se la condizione � VERA";
}
echo "eseguita se la condizione � FALSA";


echo "<br>";
echo "<hr>";

$giudizio=".";
if($voto>=18){
    $giudizio= "promosso<br>";
    echo "eseguita se la condizione � VERA";
}
echo "$giudizio";


echo "<br>";
echo "<hr>";

//prima stampavo tutti e due ...... perch� mancava ELSE ... l ALTRIMENTI

$giudizio=".";
if($voto>=18){
    $giudizio= "promosso<br>";
}else{
    $giudizio= "bocciato<br>";
}
echo "$voto: $giudizio";


echo "<br>";
echo "<hr>";

$voto=30;

$giudizio=".";
if($voto>=18){
    echo $giudizio= "promosso";
    if ($voto==30){
        echo " con massimo";
    }
}else{
    echo $giudizio= "bocciato<br>";
}



//esercizio date 2 variabili a e b fare in modo che alla fine a contenga il maggiore dei due e b altro

    ?>




</body>
</html>