<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>variabili</title>
</head>
<body>
    <?php
//le variabili hanno sempre simbolo del dollaro!!!
//questa è un istruzione che associa alla variabile "a" il valore "1". in php è un intero ..
$a = 1;
//questa è un istruzione che associa alla variabile "b" una stringa 
$b = "stringa lunga quanto voglio";
//la variabile "c" rappresenta falso. le altre variabili sono vere
$c = false;
$un_numero_a_caso = 2;
$unNumeroAcaso = 3;
//per stampare una variabile .. da notare i due modi per riuscire a stampare le virgolette dentro alle stringhe
echo 'stampo il valore della variabile "a" :' . $a . "e il valore della variabile  \" b \" : ".$b ;

    ?>




</body>
</html>