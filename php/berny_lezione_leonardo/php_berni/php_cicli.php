<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>variabili</title>
</head>
<body>
    <?php
//le variabili hanno sempre simbolo del dollaro!!!
//questa � un istruzione che associa alla variabile "a" il valore "1". in php � un intero ..
$a = 2;
//questa � un istruzione che associa alla variabile "b" una stringa 
$b = "stringa lunga quanto voglio";
//la variabile "c" rappresenta falso. le altre variabili sono vere
$c = true;

//le variabili le posso chiamare come voglio .. consigliabile con underscore o con lettere maiuscole .. no spazi
$un_numero_a_caso = "io sono" . "pippo" . "tu sei pluto";

//cosa vuol dire concatenare un numero con una stringa? cosa fa l'interprete php quando gli dico concatenare un intero con una stringa? lui fa stringa 2 concatenata a pippo
$unNumeroAcaso = 2 . "pippo";
//per stampare una variabile .. da notare i due modi per riuscire a stampare le virgolette dentro alle stringhe
echo 'stampo il valore della variabile "a" :' . $a . "<br>" . "e il valore della variabile  \" b \" : ".$b ;
echo "<br>";
echo $unNumeroAcaso;

//ricardarsi che si CONCATENA solo con il punto. Con la virgola si aggiungono le cose da stampare con echo
echo "<br>" ,'attacco con virgola la variabile "a" :' , $a , "<br>" , "alla variabile  \" b \" : ".$b ;

//se cambio la variabile $a .. cambio il numero con cui dico ... quante volte BASTAAAA!!!
echo "<br>" . "io dico BASTA $a volte!!! � l'ora giusta per andare a casa!!!";
//ma tra una virgoletta la variabile viene letta come stringa!!! quindi devo concatenare .. o meglio usare le due virgolette.. anche perch� un apostrofo pu� incasinare
echo "<br>" . 'io dico BASTA $a volte!!! � l\'ora giusta per andare a casa!!!';
echo "<br>" . 'io dico BASTA' . $a . 'volte!!!';

echo "<br><br><hr><br><br>";
//se riscrivo le variabile da questo punto in poi variano ... infatti sono VARIABILI
$a=1;
$b=2;
echo $a+$b;

echo "a=" . $a . "b=" . $b . "<br>la somma di a+b=" . $a+$b;
echo "<br>";
echo "<br> $a+$b=". $a+$b;
echo "<br>";
//bisogna mettere le operazione tra parentesi .. se non la macchina interpreta tutto quello che c'� prima del + in modo numerico (stringhe comprese) e le somma alla variabile $b .. quindi:

echo ("2"+2);
echo "<br>";
//devo usare parentesi
echo "<br> $a+$b=". ($a+$b);

echo "<br>";
echo "<hr>";

echo "<br> $a+$b=". ($a+$b);
echo "<br> $a-$b=". ($a-$b);
echo "<br> $a*$b=". ($a*$b);
echo "<br> $a/$b=". ($a/$b);
//questo � un modulo .. restituisce il resto della divisione quindi mi fa capire se il numero � pari o dispari
echo "<br> $b%$b=". ($b%$b);
echo "<br> $a%$b=". ($a%$b);

echo "<br>";
echo "<hr>";

//provo gli operatori uguale o diverso. la risposta sar� 0 se � FALSO e 1 se � VERO (numeri con cui poter fare a sua volta altre operazioni). quindi se � falso non stamper� nulla se � vero stamper� 1

echo "$a==$b?" .($a==$b);
echo "$a==$b?" .($a!=$b);

echo "<br>";
echo "<hr>";

//assegnazioni .. $c diventa uguale a $2 .... $b diventa uguale a c .. ecc...

echo $a =$b =$c = 2;
echo "<br>";

echo $a;
echo "<br>";
echo $b;
echo "<br>";
echo $c;

echo "<br>";
echo "<hr>";

$a=5;
echo " $a+1" . "il risultato successivo �: " . ($a+1);

echo "<br>";
echo "<hr>";

//$a=$a+1;
echo $a=$a+1;

echo "<br>";
echo "<hr>";

echo "qualcosa";
$a++;
echo $a;


echo "<br>";
echo "<hr>";

echo "qualcosa";
++$a;
echo $a;



echo "<br>";
echo "<hr>";

$a=1;
// $a= $a +5;
// si trover� tante volte .. � come scrivere ...
$a +=5;



echo "<br>";
echo "<hr>";


//operatore ternario -- condizione???

$voto=30;
echo "voto=$voto<br>";

echo ($voto>18)? "promosso": "bocciato";
echo ($voto==30)? " col massimo dei voti": "";



echo "<br>";
echo "<hr>";


//se il voto � trenta devo aggiungere promosso col massimo dei voti


echo ($voto>18)? "promosso" . ( ($voto==30)? " col massimo dei voti": ""): "bocciato";

echo "<br>";
echo "<hr>";



echo ($voto==30)?"promosso con massimo":(($voto>=18)?"promosso":"bocciato");

echo "<br>";
echo "<hr>";

//la condizione che l if valuta va sempre tra parentesi tonda .. nella graffa ci sono le conseguenze .. conviene metterla sempre anche se ce n'� solo una
//dentro le graffe c'� quello che si verifica SOLO SE LA CONDIZIONE che posto tra parentesi tonde.. � VERA
//fuori dalle graffe c'� quella FALSA

$voto=27;

if($voto>=18){
    echo "promosso<br>";
    echo "eseguita se la condizione � VERA";
}
echo "eseguita se la condizione � FALSA";


echo "<br>";
echo "<hr>";

$giudizio=".";
if($voto>=18){
    $giudizio= "promosso<br>";
    echo "eseguita se la condizione � VERA";
}
echo "$giudizio";


echo "<br>";
echo "<hr>";

//prima stampavo tutti e due ...... perch� mancava ELSE ... l ALTRIMENTI

$giudizio=".";
if($voto>=18){
    $giudizio= "promosso<br>";
}else{
    $giudizio= "bocciato<br>";
}
echo "$voto: $giudizio";


echo "<br>";
echo "<hr>";

$voto=30;

$giudizio=".";
if($voto>=18){
    echo $giudizio= "promosso";
    if ($voto==30){
        echo " con massimo";
    }
}else{
    echo $giudizio= "bocciato<br>";
}



echo "<br>";
echo "<hr>";

//per ottenere la stessa cosa senza else

$voto=27;
$giudizio="bocciato";
if($voto=18){
    $giudizio="promosso";
}
echo "$voto: $giudizio";



echo "<br>";
echo "<hr>";



//esercizio date 2 variabili a e b fare in modo che alla fine a contenga il maggiore dei due e b altro
$voto=30;

if($voto>=18){
    $giudizio="promosso";
    if($voto == 30){
        $giudizio="$giudizio con massimo dei voti";
    }
}
echo "$voto:$giudizio";


echo "<br>";
echo "<hr>";


$voto=30;

if($voto>=18){
    $giudizio="promosso";
    if($voto == 30){
        $giudizio .= " con massimo dei voti";
    }
}
echo "$voto:$giudizio";


echo "<br>";
echo "<hr>";

$voto=40;
if($voto<1 || $voto >30){
    echo "il voto deve essere compreso tra 1 e 30<br>";
} else {
    if($voto>=18){
        $giudizio="promosso";
        if($voto == 30){
            $giudizio .= " con massimo dei voti";
        }
        echo "$voto:$giudizio";
    }
}




echo "<br>";
echo "<hr>";

$voto=100;
$massimo=100;
$giudizio="bocciato";
$sufficienza=$massimo/10*6; //ancora non so come si calcola

if($voto<1 || $voto >$massimo){
    echo "il voto deve essere compreso tra 1 e 30<br>";
} else {
    if($voto>=$sufficienza){
        $giudizio="promosso";
        if($voto == $massimo){
            $giudizio .= " con massimo dei voti";
        }
        echo "$voto:$giudizio";
    }
}




echo "<br>";
echo "<hr>";

//scambiare $a e $b in modo che $a sia maggiore di $b
$a=0;
$b=1;

if ($a<$b){
    $c=$a;
    $a=$b;
    $b=$c;
}
echo ($a. " � maggiore di " . $b );




echo "<br>";
echo "<hr>";

// 2 fratelli hanno $f1 e $f2 anni: calcolare la differenza di con if e con operatore ternario

$f1=40;
$f2=40;

if ($f1==$f2){
    echo "stessa et�";
}else{
    if ($f1>$f2){
        echo $f1-$f2;
    }else{
        echo $f2-$f1;
    }
   
}





echo "<br>";
echo "<hr>";

//stessa cosa ma con operatore ternario

echo "la differenza � " . ( ($f1>$f2)?($f1-$f2):($f2-$f1) );




echo "<br>";
echo "<hr>";



echo date("d-m-Y");


echo "<br>";


//quanti giorni mancano alla fine del mese? echo date("t");

// leggo il numero dei giorno (23) 
$giorno= date("d");

// leggo il numero del mese (11) 
$mese=date("n");

// ricavo il numero di giorni del mese (30)
$giorni_mese=31;

if (($mese==11) || ($mese==4) || ($mese==6) || ($mese==9)){
    $giorni_mese=30;
}else{
    if ($mese==2){
        $giorni_mese=28; //o 29.. DOVRO GESTIRE L ANNO BISESSTILE   
    }
}

// numero dei giorni del mese - numero giorno (30-23)
echo "mancano " . $giorni_alla_fine_del_mese=$giorni_mese-$giorno . " giorni alla fine del mese";



echo "<br>";
echo "<hr>";


//stesso esercizio ma calcolo l anno bisestile


$giorno= date("d");

$mese=date("n");

$giorni_mese=31;

if (($mese==11) || ($mese==4) || ($mese==6) || ($mese==9)){
    $giorni_mese=30;
}else{
    if ($mese==2){
        $giorni_mese=28; //o 29.. DOVRO GESTIRE L ANNO BISESTILE   
        if (date("Y") % 4== 0)
// oppure utilizzo il not con "!"   --->      if (! date("Y") % 4)
        $giorni_mese=29;
    }
}

echo "mancano " . $giorni_alla_fine_del_mese=$giorni_mese-$giorno . " giorni alla fine del mese <br>";


echo "<br>";
echo "<hr>";

//lo switch valuta un valore all ingresso e a seconda dei vari casi che andiamo ad elencare attiver� 
//prende in esame il contenuto di una variabile .. ha la stessa funzione dell if
//si usa solo quando la condizione di valutare � quella di ugualianza
//quando trovo un caso che corrisponde all amia variabile tutti gli altri casi non sono validi


$giorno= date("d");

$mese=date("n");

$giorni_mese=31;

switch ($mese){
    case 11 :
    case 4 :
    case 6 :
    case 9 :
        $giorni_mese=30;
    case 2 :
        $giorni_mese=28;
            if (! date("Y") % 4)
                $giorni_mese=29;
            break;
    default: $giorni_mese=31;

}

echo "switch; mancano $giorni_alla_fine_del_mese  $giorni_mese-$giorno " . ($giorni_alla_fine_del_mese=$giorni_mese-$giorno) . " giorni alla fine del mese";



echo "<br>";
echo "<hr>";

//CICLI
// il for dice .. per tante volte eseguimi quello che c'� scritto dopo

$i=1;

for ($i; $i<=40; $i++){
    echo "ciao " . $i . " ";
    if (! ($i % 2)){
        echo "<br>";
    }
}



echo "<br>";
echo "<hr>";

//CICLI
// il for dice .. per tante volte eseguimi quello che c'� scritto dopo

$i=20;

for ($i; $i<=40 ; $i+=2){
    echo "ciao " . $i . " ";
    if (! (($i-2) % 4)){
        echo "<br>";
    }
}

echo "<br>";
echo "<hr>";

//esercizi un ciclo for con $i che stampa solo i numeri pari dopo il 20


$volte=40;
$numero=20;

for ($numero; $numero<=$volte ; $numero+=2){
    echo "ciao " . $numero . " ";
    if (! (($numero-2) % 4)){
        echo "<br>";
    }
}


    
echo "<br>";
echo "<hr>";

//stampare da un numero di consegna generico .. in modo pari .. quanti numeri voglio

$n_numero_richiesti=16;
$numero=3;
//lo zero equivale a un FALSE .. anche una stringa vuota ... 
$cont_pari=0;
//la riga sotto .. è il resto della divisione per 2 mi serve per dare numeri pari
if ($numero%2){
    $numero=$numero+1;
}else{
    echo "è già pari<br>";
}

for ($i=0; $i<($n_numero_richiesti); $i++){

    $num=$numero+($i*2);
    echo "$num<br>";

    //questa parte sotto mi serve solo a mettere <br> ogni tot di numeri stampati nell esempio sotto descrizione di come ho fatto
    $cont_pari=$cont_pari+1;
    if(!($cont_pari%2))
    echo "<br>";
}

echo "<br>";
echo "<hr>";
echo "<br>ESERCIZIO TABELLINE<br>";

$n_numero_richiesti=16;
$numero=2;
//lo zero equivale a un FALSE .. anche una stringa vuota equivale a FALSE... 
$cont_pari=0;
//la riga sotto .. è il resto della divisione per 2 mi serve per dare numeri pari ..  
if ($numero%2){
    $numero=$numero+1;
}else{
    echo "è già pari<br>";
}
$num=$numero;
for ($i=0; $i<($n_numero_richiesti); $i++){
    echo ($num)."<br>";
    $num=$num+2;
    // $num=$numero+($i*2);
    // echo "$num<br>";
    $cont_pari=$cont_pari+2;
    if(!($cont_pari%2))
        echo "<br>";
}


echo "<br>";
echo "<hr>";


$n_numero_richiesti=10;
$numero=0;
//lo zero equivale a un FALSE .. anche una stringa vuota ... 
$cont_pari=0;
$var=0;
$num=$numero;
for ($i=0; $i<($n_numero_richiesti); $i++){
    echo ($num)."<br>";
    $num=$num+6;
    // $num=$numero+($i*2);
    // echo "$num<br>";
    $cont_pari=$cont_pari+2;
    if(!($cont_pari%2))
        echo "<br>";
}



echo "<br>";
echo "<hr>";

$n_numero_richiesti=10;
$numero=10;
//lo zero equivale a un FALSE .. anche una stringa vuota ... 
$cont_pari=0;
$num=$numero;
echo "0<br>";
for ($i=0; $i<($n_numero_richiesti); $i++){
    echo ($num)."<br>";
    $num=$num+$numero;
    // $num=$numero+($i*2);
    // echo "$num<br>";
}



echo "<br>";
echo "<hr>";

$n_numero_richiesti=11;
$numero=3;
//lo zero equivale a un FALSE .. anche una stringa vuota ... 
for ($i=1; $i<($n_numero_richiesti); $i++){
    $num=$numero*$i;
    echo $num."<br>";
}





echo "<br>";
echo "<hr>";


$n_numero_richiesti=10;
$numero=3;
//lo zero equivale a un FALSE .. anche una stringa vuota ... 
for ($i=1; $i<=($n_numero_richiesti); $i++){
    $num=$numero*$i;
    echo $num."<br>";
}




echo "<br>";
echo "<hr>";

echo "<br>ESERCIZIO QUANTO MANCA A NATALE<br>";


// leggo il numero dei giorno (23) 
$giorno= date("d");

// leggo il numero del mese (11) 
$mese=date("n");

// ricavo il numero di giorni del mese (31)
$giorni_mese=31;

if (($mese==11) || ($mese==4) || ($mese==6) || ($mese==9)){
    $giorni_mese=30;
}else{
    if ($mese==2){
        $giorni_mese=28; //o 29.. DOVRO GESTIRE L ANNO BISESSTILE   
    }
}

// numero dei giorni del mese - numero giorno (30-23)
echo "mancano " . $giorni_alla_fine_del_mese=$giorni_mese-$giorno . " giorni alla fine del mese";

//se i giorni sono prima o dopo Natale calcolami quanti giorni di differenza ci sono a Natale
//conto i numero dei mesi da qui a natale


echo "<br>";
echo "<hr>";



?>


</body>
</html>