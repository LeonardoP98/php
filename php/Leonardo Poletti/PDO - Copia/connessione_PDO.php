<?php
$host      = "localhost";
$user      = "root";
$pass      = "";
$dbname    = "ifts";

$dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
try {
    //1. connessione
    $connessione = new PDO($dsn, $user, $pass);

    /*$cognome="Rossi";*/
    $cognome="%".$_REQUEST['cognome']."%";
    

    //$sql="SELECT cognome, nome as NO FROM clienti /*WHERE cognome=\"$cognome\"*/";/*\"d'antonio\"*/
    $sql="SELECT cognome FROM clienti WHERE (cognome LIKE :cognome);";
    echo $sql."<br>";
    // "SELECT * FROM utenti WHERE id = :id LIMIT 1"

    //2. preparazione della query
    $st = $connessione->prepare($sql);
    // $id = 2;

    //3. bindParam
    $st->bindParam(':cognome',$cognome);

    //4. esecuzione
    $st->execute();
    $records=$st->fetchAll(PDO::FETCH_ASSOC);
    //print_r($records);

} catch(PDOException $e) {    
    die("Errore durante la connessione al database!: ". $e->getMessage());
}

//echo "estratti n. record = ".count($records)."<hr>";
// echo "<pre>";
// print_r($records);
// echo "</pre>";
$i=1;
foreach($records as $value){
    echo $i++.")".$value['cognome']."<br>";
}
?>