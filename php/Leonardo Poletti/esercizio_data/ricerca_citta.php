<?php
$host      = "localhost";
$user      = "root";
$pass      = "";
$dbname    = "ifts";

$dsn = 'mysql:host=' . $host . ';dbname=' . $dbname;
try {
    //1. connessione
    $connessione = new PDO($dsn, $user, $pass);

    $area="%".$_REQUEST['ricerca']."%";
    $sql="SELECT regione, area_geografica FROM ifts.regioni WHERE (regione LIKE :area);";
    echo $sql;

    //2. preparazione della query
    $st = $connessione->prepare($sql);

    //3. bindparam
    $st->bindParam(':area',$area);


    //4. esecuzione
    $st->execute();
    $records=$st->fetchAll(PDO::FETCH_ASSOC);
    //print_r($records);

} catch(PDOException $e) {    
    die("Errore durante la connessione al database!: ". $e->getMessage());
}


$i=1;
echo "<table>";
foreach($records as $value){
    echo "<tr><td>".$value['regione']."</td><td>".$value['area_geografica']."</td></tr>";
}
echo "</table>";

?>