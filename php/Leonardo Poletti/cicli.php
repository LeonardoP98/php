<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lezione1-2</title>
</head>
<body>
    <?php

    for($i=1/*inizio*/;$i<=10/*condizione*/;$i++/*fine iterazione*/){  //ciclo
        echo $i."<br>";
        if(!($i%2)){
            echo"<br>";
        }
    }

    echo "<hr>";

    for ($i=20;$i<40;$i=$i+2){
        echo $i."<br>";
        if(!(($i-2)%4)){
            echo"<br>";
        }
    }

    echo "<hr>";

    $numero=7;
    if($numero%2){
        $numero=$numero+1;
    }
    else{
        echo "È già pari<br>";
    }
    for ($i=0;$i<10;$i++){//stampoi i numeri pari che vengono dopo il $numero
        echo (($i*2)+$numero)."<br>";
        if((($i-2)%2)){
            echo"<br>";
        }
    }

    echo "<hr>";

    echo "<h3>Cicli while</h3>";//while

    echo "<hr>";

    $i=1;
    while($i<=5){
        echo "Pippo $i <br>";
        $i=$i+1;
    }

    echo "<hr>";

    $n_num_richiesti=50;
    $numero=2;
    $stop=20;
    if($numero%2){
        $numero++;
    }
    $n=$numero;
    for($i=0;$i<($n_num_richiesti);$i++){
        if($n>=$stop){
            echo "fine<br>";
            break;//pone la fine del ciclo a prescindere
        }
        else{
            echo $n."<br>";
            $n=$n+2;
        }
    }

    echo "<hr>";
    
    //stampo i nmumeri pari dopo $numero fino $stop
    //continue salta il giro successivo
    $n_num_richiesti=50;
    $numero=2;
    $stop=30;
    $brutto=10;
    if($numero%2){
        $numero++;
    }
    $n=$numero;
    for($i=0;$i<($n_num_richiesti);$i++){
        if($n==$brutto){
            $n=$n+2;
            continue;
        }
        if($n>=$stop){
            echo "fine<br>";
            break;//pone la fine del ciclo a prescindere
        }
        else{
            echo $n."<br>";
            $n=$n+2;
        }
    }

    echo "<hr>";
    
    //es. quanti sono i multipli di 5 da $a a $b
    $a=130;
    $b=15;
    $d=5; //multipli da verificare
    if ($b>$a){//$a sarà sempre il maggiore
        $c=$b;
        $b=$a;
        $a=$c;
    }
    $cont=0;
    for ($i=$b;$i<=$a;$i++){
        if(!($i%$d)){
            echo ((($i-$b)/5)+1).")".$i."<br>";
            $cont++;
        }
    }
    echo "Ci sono ".$cont." multipli di $d tra $b e $a compresi";

    echo "<hr>";

    $a=130;
    $b=15;
    $d=5; //multipli da verificare
    if ($b>$a){//$a sarà sempre il maggiore
        $c=$b;
        $b=$a;
        $a=$c;
    }
    $b=$b+$d-($b%$d);
    echo "min $b<br>";
    $cont=0;
    for ($i=$b;$i<=$a;$i=$i+5){
        echo ((($i-$b)/5)+1).")".$i."<br>";
        $cont++;
    }
    
    echo "Ci sono ".$cont." multipli di $d tra $b e $a compresi";

    echo "<hr>";

    
    ?>
</body>
</html>