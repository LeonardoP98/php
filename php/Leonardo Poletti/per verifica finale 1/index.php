<!-- Si chiede di realizzare una sezione del sito dell'Enaip che permetta ai navigatori di consultare i corsi disponibili.
In particolare, le operazioni richieste sono le seguenti:

•	realizzare il db che gestisce le informazioni sui corsi (nome, descrizione, data di inizio, 
    per lavoratori, per disoccupati, età minima per la frequenza, n. di visualizzazioni) 
•	realizzare il form iniziale compilato dal navigatore per cercare i corsi disponibili per lui, 
    che prevede 2 campi di input: l'età e se è lavoratore o disoccupato
•	fornire il numero dei corsi disponibili in base ai campi di input ed elencarli (solo quelli che devono 
    ancora iniziare)
•	ogni volta che un corso viene visualizzato nell'elenco, incrementare di una unità il n. di visualizzazioni
•	scrivere in fondo il nome di un corso consigliato dai navigatori, cioè quello maggiormente visualizzato.

Importante: definire e utilizzare almeno una funzione a scelta. -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <form action="ricerca.php" method="POST" class="form-horizontal" role="form">
            <div class="form-group">
                <legend>Dati utente</legend>
            </div>

            <label for="eta">inserire l'eta</label>
            <input type="number" name="eta" id="eta">
            <label for="eta">inserire l'eta</label>
            
            <select name="occupazione" id="occupazione">
                <option value="0">lavoratore</option>
                <option value="1">disoccupato</option>
            </select>
            
            <button type="submit" class="btn btn-primary">Ricerca</button>
            
    </form>
    
</body>
</html>