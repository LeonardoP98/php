<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leonardo Poletti verifica</title>
</head>
<body>
    <?php
    echo "<h3>Punto 1</h3>";
    $start=12;   //numero di partenza
    $repeat=17;  //numero di ripetizioni del ciclo
    $n=$start;
    $cont_pari=0;
    $num_data_mese=DATE("m");
    for ($i=0;$i<$repeat;$i++){
        if($i%2){
            if($n==$num_data_mese){
                echo "<p style=\"background-color:cyan\"><i>$n</i> e il suo doppio è:<span style=\"color:red\">".($n*2)."</span></p>";
            }
            else{
                echo "<p style=\"background-color:cyan\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
        }
        else{
            if($n==$num_data_mese){
                echo "<p style=\"background-color:lightgrey\"><i>$n</i> e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
            else{
                echo "<p style=\"background-color:lightgrey\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
        }
        if (!($n%2)){
            $cont_pari++;
        }
        $n--;
    }

    echo "<hr>";
    echo "<h3>Punto 2</h3>";
    echo "\$start:$start  \$repeat:$repeat <br>";
    echo "sono stati scritti ".($cont_pari+$i)." numeri pari<br>";
    echo "sono state eseguite ".$i." ripetizioni";
    
    echo "<hr>";
    echo "<h3>Punto 3</h3>";
    $start=12;   //numero di partenza
    $repeat=17;  //numero di ripetizioni del ciclo
    $n=$start;
    $cont_pari=0;
    $num_data_mese=DATE("m");
    for ($i=0;$i<$repeat;$i++){
        if($n<0){
            break;
        }
        else{
            if($i%2){
                if($n==$num_data_mese){
                    echo "<p style=\"background-color:cyan\"><i>$n</i> e il suo doppio è:<span style=\"color:red\">".($n*2)."</span></p>";
                }
                else{
                    echo "<p style=\"background-color:cyan\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
                }
            }
            else{
                if($n==$num_data_mese){
                    echo "<p style=\"background-color:lightgrey\"><i>$n</i> e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
                }
                else{
                    echo "<p style=\"background-color:lightgrey\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
                }
            }
        }
        if (!($n%2)){
            $cont_pari++;
        }
        $n--;
    }

    echo "<hr>";
    echo "<h3>Punto 3 parte 2</h3>";
    echo "\$start:$start  \$repeat:$repeat <br>";
    echo "sono stati scritti ".($cont_pari+$i)." numeri pari<br>";
    echo "sono state eseguite ".$i." ripetizioni";
    
    echo "<hr>";
    echo "<h3>Facoltativo</h3>";
    $start=12;   //numero di partenza
    $repeat=17;  //numero di ripetizioni del ciclo
    $n=$start;
    $cont_pari=0;
    $num_data_mese=DATE("m");
    $i=0;
    while ($i<$repeat){
        $i++;
        if($i%2){
            if($n==$num_data_mese){
                echo "<p style=\"background-color:cyan\"><i>$n</i> e il suo doppio è:<span style=\"color:red\">".($n*2)."</span></p>";
            }
            else{
                echo "<p style=\"background-color:cyan\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
        }
        else{
            if($n==$num_data_mese){
                echo "<p style=\"background-color:lightgrey\"><i>$n</i> e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
            else{
                echo "<p style=\"background-color:lightgrey\">$n e il suo doppio è:<span style=\"color:red\"> ".($n*2)."</span></p>";
            }
        }
        if (!($n%2)){
            $cont_pari++;
        }
        $n--;
    }
    ?>
</body>
</html>