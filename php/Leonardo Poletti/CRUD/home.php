<?php
    session_start();
    
    include("inc/funzioni.php");
    loggato();
    include("inc/Database.php");
    
    $dbo= new database();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<a href="logout.php">Logout(<?php echo $_SESSION['ute_nome'];?>)</a>
<br>
ELENCO FATTURE
<table>
    <tr>
        <td>numero</td>
        <td>data</td>
        <td>cliente</td>
        <td>importo</td>
        <td>azioni</td>

    </tr>

    <?php
        $sql="SELECT * FROM fatture
        INNER JOIN clienti ON fat_cli_id= cli_id
        ORDER BY fat_data, fat_numero;";
        $dbo->query($sql);
        $fatture=$dbo->resultset();

        foreach($fatture as $fattura){
            $importo = $fattura['fat_importo'];
    ?>

    <tr>
        <td><?php echo $fattura['fat_numero']?></td>
        <td><?php echo $fattura['fat_data']?></td>
        <td><?php echo $fattura['cli_ragsoc']?></td>
        <td><?php echo number_format($importo, 2, "," , ".")?></td>
        <td><a href="delete.php?id=<?php echo $fattura['fat_id']?>">x</a></td>
    </tr>

    <?php
        }
    ?>
</table>
    
</body>
</html>