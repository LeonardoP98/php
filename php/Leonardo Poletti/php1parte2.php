<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lezione1-2</title>
</head>
<body>
    <?php
    $a=30;
    $b="stringa lunga quanto voglio";
    $c=true; //true
    echo "ciao a tutti<br>";
    $un_testo_a_caso="io sono "."Pippo"." e tu sei pluto";
    $un_numero_a_caso=2 . "pippo"; //tipi di scrittura delle variabili
    $unNumeroACaso=2;
    echo "a=\"".$a."\"<br>b=".$b."<br>c=".$c;
    echo "<br>un_numero_a_caso=".$un_numero_a_caso;
    echo "<br> UnNumeroACaso=".$unNumeroACaso;
    echo "<br>un-testo-a-caso=".$un_testo_a_caso;
    echo "<br> buona giornata<hr>";
    echo "Io dico \"BASTA\" $a volte! è l'ora giusta per andare a casa!"."<br>";
    echo 'Io dico "BASTA" '.$a.' volte! è l\'ora giusta per andare a casa!'."<br>";
    echo"<hr>";
    echo ("1"+2)."<br>";
    $a=date("s");
    $b=60-date("s");
    echo $a." + ".$b." = ".($a+$b)."<br>";
    echo "$a + $b = " . ($a+$b)."<br>";
    echo "$a - $b = " . ($a-$b)."<br>";
    echo "$a * $b = " . ($a*$b)."<br>";
    echo "$a / $b = " . ($a/$b)."<br>";
    echo "$a % $b = " . ($a%$b)."<br>"; //resto(modulo)
    $a=1;
    $b="1";
    echo "$a == $b? " . ($a==$b)."<br>";//confronto valore
    echo "$a === $b? " . ($a===$b)."<br>";//confronto valore e tipo variabile
    echo "$a != $b? " . ($a!=$b)."<br>";//diverso
    echo "<hr>";
    echo "\$a=$a";
    $a++;
    echo " \$a+1=".$a."<br>";
    echo '$a='.$a." e il successivo è ". ($a=$a+1);
    echo '<br> $a='.$a;
    echo '<br>$a='.$a." e il successivo è ".(++$a);
    echo "<hr>";
    $a=1;
    $a=$a+5;
    echo "\$a=$a <br>";
    $a=1;
    $a+=5;
    echo "\$a=$a <br>";
    //operatore ternario
    $voto=301;
    //Mio
    echo "voto=$voto<br>";
    echo (0<=$voto && $voto<18)? "bocciato":"";
    echo ($voto>17 && $voto<30)? "promosso":"";
    echo ($voto==30)? "promosso col massimo dei voti":"";
    echo ($voto>=0 && $voto<=30)?"":"errore";
    echo "<br>";
    //Carol
    if($voto>=0 && $voto<=30){
        echo ($voto<18)?"bocciato":"promosso".(($voto==30)?" col massimo dei voti":"");
        echo "<br>";
    }
    else{
        echo "errore";
    }
    echo "<br>";
    // Bernardo
    if($voto>=0 && $voto<=30){
        echo ($voto<18)?"bocciato":"promosso";
        echo ($voto==30)?" col massimo dei voti":"";
    }
    else{
        echo "errore";
    }
    echo "<hr>";
    //quanto manca alla fine del mese
    // echo "Oggi è il ".date("j-m");
    // $giorno=date("j");
    // echo "<br>ultimo giorno ".date("t");
    // $mese=date("t");
    //echo "<br>a fine mese mancano ".($mese-$giorno).((($mese-$giorno)>1)?"giorni":"giorno";)
    echo "Oggi è il ".date("j-m");
    $mese=date("m");
    $anno=date("Y");
    $giorni_mese=31;
    if($mese==11 || $mese==4 || $mese==6 || $mese==8){
        $giorni_mese=30;
    }
    else{
        if($mese==2){
            $giorni_mese=28;
            if(($anno%4)==0){
                $giorni_mese=29; //o 29;
            }
        }
    }
    echo "<br>mancano ".($giorni_mese-date("j"))." ".(($giorni_mese-date("j")==1)?"giorno":"giorni");
    echo "<hr>";

        echo "Oggi è il ".date("j-m");
    $mese=date("m");
    $anno=date("Y");
    $giorni_mese=31;
    if($mese==11 || $mese==3 || $mese==6 || $mese==8){
        $giorni_mese=30;
    }
    else{
        if($mese==2){
            $giorni_mese=29;
            if($anno%4){
                $giorni_mese=28; //o 29;
            }
        }
    }
    echo "<br>mancano ".($giorni_mese-date("j"))." ".(($giorni_mese-date("j")==1)?"giorno":"giorni");
    echo "<hr>";
    $mese=date("n");
    switch($mese){
        case "pippo": echo "ma che cavolo di mese è questo!!!";//stringa che non andrà mai in esecuzione
        case 11 :
        case 4 :
        case 9 :
        case 6 :
            $giorni_mese=30;
            break;
        case 2 :
            $giorni_mese=29;
            if($anno%4)
                $giorni_mese=28;
            break;
        default:
            $giorni_mese=31;
            break;
    }
    echo "giorni del mese: $giorni_mese";
    echo "<hr>";
    for($i=1/*inizio*/;$i<=10/*condizione*/;$i++/*fine iterazione*/){  //ciclo
        echo ($i)."<br>";
        if(($i%2)==0){
            echo"<br>";
        }
    }
    echo "<hr>";
    ?>
</body>
</html>