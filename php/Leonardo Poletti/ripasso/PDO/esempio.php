<?php 
	require ('Database.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Documento senza titolo</title>
</head>

<body>

<?php
	//IMPORTANTE: creo un'istanza dell'oggetto Database
	$dbo = new Database(); 
	
	
	/*
		dump della tabella utilizzata:
		CREATE TABLE `utenti` (
			`ute_id` int(3) NOT NULL AUTO_INCREMENT,
			`ute_nome` varchar(256) NOT NULL,
			`ute_cognome` varchar(256) NOT NULL,
			`ute_cf` varchar(16) DEFAULT NULL,
			PRIMARY KEY (`ute_id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
	*/
	
?>

<?php
	//ESEMPIO DI INSERIMENTO
	
	$nome="Mario";
	$cognome="Rossi";
	$cf="RSSMRA85T10A562S";
	
	
    $sql="INSERT INTO utenti (ute_cf, ute_nome, ute_cognome ) VALUES (:ute_cf,:ute_nome,:ute_cognome) ";
    $dbo->query($sql);
    $dbo->bind(':ute_cf',$cf);
    $dbo->bind(':ute_nome',$nome);
    $dbo->bind(':ute_cognome',$cognome);
    $dbo->execute();

    $ute_id=$dbo->lastInsertId();

?>
Inserito utente con id =<?php echo $ute_id?>
<br />

<?php 
	//ESEMPIO DI AGGIORNAMENTO
	
	$sql="UPDATE utenti SET ute_nome='Luigi', ute_cognome='Bianchi' WHERE ute_id=:ute_id ";
	$dbo->query($sql);
	$dbo->bind(':ute_id', $ute_id);
	$dbo->execute();
?>                
Aggiornato utente con id =<?php echo $ute_id?>
<br />


<?php 
	//ESEMPIO DI SELEZIONE DI 1 RECORD SINGOLO

	$sql="SELECT * FROM utenti WHERE ute_id = :id  ";
	$dbo->query($sql);
	$dbo->bind(':id', $ute_id);
	
	$row=$dbo->single(); //1 solo record!
	
	if(!empty($row)){
		$ute_nome=$row['ute_nome'];
		$ute_cognome=$row['ute_cognome'];
	}
?>
Ora l'utente con id =<?php echo $ute_id?> si chiama <?php echo $ute_nome." ".$ute_cognome;?>
<br />

<?php
	$sql="SELECT * FROM utenti "; //WHERE ....
	$dbo->query($sql);
	$rows = $dbo->resultset(); //un insieme di record
	
	foreach($rows as $row) {
		extract($row); //crea 1 variabile per ogni elemento dell'array, con nome la chiave associativa dell'array  
		
		//nel ciclo stampo i dati degli utenti estratti con la query di selezione
	}

?>




</body>
</html>