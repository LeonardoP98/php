<?php 
	define ("CITTA","Rimini"); //visibilità globale
	echo "Benvenuti a ".CITTA."<br>";

    class Persona {
        // proprietà 
        private $nome = ""; 
        private $cognome="";
        private $natoil="";
        const ENAIP = "Fondazione En.A.I.P. S. Zavatta "; 
 
        //costruttore (metodo magico)
        public function __construct($nome,$c){
                //inizializzare della variabile $name   
                //echo "***********costrutore**************<br>";
                $this -> nome = $nome;
                $this -> cognome = $c;
        }
        public function setNatoil($data){
            $this -> natoil = $data;
        }
                             
        //metodi
        public function getNome() {
            //$this rappresenta l'oggetto che sarà costruito a runtime
            return $this -> nome;
        }
        public function getCognome() {
            //$this rappresenta l'oggetto che sarà costruito a runtime
            return $this -> cognome;
        }
        public function getNomeCognome() {
            //$this rappresenta l'oggetto che sarà costruito a runtime
            return $this -> nome." ".$this -> cognome;
        }
        public function getCognomeNome() {
            //$this rappresenta l'oggetto che sarà costruito a runtime
            return $this -> cognome." ".$this -> nome;
        }
        public function stampaTutto(){
        	echo $this->getNome()." di età ".$this->getEta()."<br> presso: ".self::ENAIP.CITTA."<br>"; 
        }
        public function getNatoil() {
            //$this rappresenta l'oggetto che sarà costruito a runtime
            return $this -> Natoil;
        }
        public function getEta(){
            if(empty($this->natoil)){
            return "data non presente";
            }
            $data=array();
            $data=explode('-',$this -> natoil);
            $eta=date('Y')-$data[0]-1;
            if($data[1]<date('m')){
                $eta=$eta+1;
            }else{
            if($data[2]<date('d') && $data[1]=date('m')){
                    $eta=$eta+1;
                }
            }
            if ($eta>=0)
            return $eta;
            return "data non corretta";
            
        }
        
          
   }
?>                 

<?php        
        //istanzio 2 nuovi oggetti della classe Persona
        $studente = new Persona("Mario","Rossi");
        $studente -> setNatoil("2000-012-20");
        $docente = new Persona("Giacome","Verdi");       
 ?>                         
        Studente: <?php echo $studente->getNome()?><br>
        Docente: <?php echo $docente->getNome()?><br>
        
        Metodo "stampaTutto": <?php echo $docente->stampaTutto()?><br>
        
        Corso <?php echo $studente::ENAIP ?><br>
        Corso <?php echo Persona::ENAIP ?><br>
        <br>

           
       getNome Studente: <?php echo $studente->getNome()?><br>
       getCognome Studente: <?php echo $studente->getCognome()?><br>
       getNomeCognome: <?php echo $studente->getNomeCognome()?><br>
       getCognomeNome: <?php echo $studente->getCognomeNome()?><br>
       getEta: di studente <?php echo $studente->getEta()?><br>
       getEta: di docente <?php echo $docente->getEta()?><br>