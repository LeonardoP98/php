<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
    tr:nth-child(odd){
            background-color:#ccc;
        }
        th{
            background-color:lightyellow;
        }
        table{
            border:1px;
            color: solid black;
        }
        </style>
</head>
<body>
    <?php
    
    $prodotto1=array('cod'=>1235, 'nome'=>"roba1",'prezzo'=>45);
    $prodotto2=array('cod'=>5431, 'nome'=>"roba2",'prezzo'=>45);
    $prodotto3=array('cod'=>6463, 'nome'=>"roba3",'prezzo'=>45);
    $prodotti=array ($prodotto1, $prodotto2, $prodotto3);
    $c1=array('cod'=>5431,'qty'=>20);
    $c2=array('cod'=>6463,'qty'=>1);
    $carrello=array($c1, $c2);
    echo "<table>
        <tr>
        <th>cod</th>
        <th>nome</th>
        <th>qty</th>
        <th>prezzo</th>
        </tr>";
    foreach($carrello as $key=>$val){
        $cod=$carrello[$key]['cod'];
        echo "<tr>";
        foreach($prodotti as $chiave=>$valore){
            if($cod==$prodotti[$chiave]['cod']){
                echo "<td>".$valore['cod']."</td>";
                echo "<td>".$valore['nome']."</td>";
                echo "<td>".$carrello[$key]['qty']."</td>";
                echo "<td>".$valore['prezzo']."</td>";
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "<pre>";
    print_r($carrello);
    echo "</pre>";
    if(array_key_exists('cod', $c1)){
        echo "la chiave esiste";
    }
    ?>
</body>
</html>