<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php //libreria
    function sistema_data($data){
        $data=explode('-',$data);
        $aggiustato=array($data[2],$data[1],$data[0]);
        
        return $aggiustato;
    }

    function sistema_data_stringa($data){
        $data2=array();
        $data2=explode('-',$data);
        $stringa=$data2[2]."-".$data2[1]."-".$data2[0];
        return $stringa;
    }

    function scrivi_evento($url,$evento,$arrdata){
        echo "<ul><li>
        <a href=\"".$url."\">
        Festa di ".$evento." ".
        "</a>".
        $arrdata[0].'-'.$arrdata[1].'-'.$arrdata[2]."</li></ul>";//data
    }
    function scrivi_evento2($url,$evento,$data){
        if($data>date('Y-m-d')){
        echo "<ul><li>
        <a href=\"".$url."\">
        Festa di ".$evento.
        "</a>";
        echo sistema_data_stringa($data);
        echo "</li></ul>";
        }
    }
    function scrivi_evento3($var){
        $url=$var['url'];
        $evento=$var['titolo'];
        $data=$var['data'];
        if($data>date('Y-m-d')){
        echo "<ul><li>
        <a href=\"".$url."\">
        Festa di ".$evento.
        "</a>";
        echo sistema_data_stringa($data);
        echo "</li></ul>";
        }
    }

    function evento_futuro($evento){
        if($evento['data']>=date('Y-m-d'))
            return true;
        return false;
    }
    ?>


    <?php
    $event=array(
        array('data'=>'2018-12-25','titolo'=>'Natale','url'=>'www.cose1.it'),
        array('data'=>'2018-10-07','titolo'=>'Annuca','url'=>'www.cose2.it'),
        array('data'=>'2018-12-28','titolo'=>'Pasqua','url'=>'www.cose3.it')
    );
    echo "<h2>Eventi</h2>";
    foreach($event as $key=>$value){
        $data=array();
        $data=sistema_data($event[$key]['data']);
        scrivi_evento($event[$key]['url'],$event[$key]['titolo'],$data);
    }

    echo "<hr>";
    echo "<h2>Eventi</h2>";

    foreach($event as $key =>$evento){
        $data=sistema_data_stringa($event[$key]['data']);
        scrivi_evento2($event[$key]['url'],$event[$key]['titolo'],$data);
    }
    echo "<hr>";
    echo "<h2>Eventi</h2>";
    foreach($event as $key =>$evento){
        if(evento_futuro($evento)){
            $data=sistema_data_stringa($event[$key]['data']);
            scrivi_evento2($event[$key]['url'],$event[$key]['titolo'],$data);
        }
    }
    echo "<hr>";
    echo "<h2>Eventi</h2>";
    $sort=array();
    for($i=0;$i<count($event);$i++){
        if(evento_futuro($event[$i]))
            $sort[]=$event[$i]['data'];
    }
    print_r($sort);
    asort(($sort));
    echo "<br>";
    print_r ($sort);
    foreach($sort as $key =>$valore){
        $evento=$event[$key];
        scrivi_evento3($evento);
    }
    ?>

</body>
</html>