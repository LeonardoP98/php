<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php    //esercizio numeri pari
    $n_num_richiesti=7;
    $numero=42;
    $cont_pari=0;
    for($i=0;$i<(2*$n_num_richiesti);$i++){
        $num=$numero+$i;
        if(!($num%2)){
            echo "$num<br>";
            $cont_pari=$cont_pari+1;
            if(!($cont_pari%2))
                echo "<br>";
        }
    }

    echo "<hr>";
    
    $n_oggi=date("d");//es giorni mancanti a Natale
    $mese=date("m");
    $giorni=0;
    echo "Oggi è il $n_oggi/$mese/".date("Y")."<br>";
    if($mese==12 && ($n_oggi>=26 && $n_oggi<=31)){//date dal 26 al 31 dicembre
        $giorni=359+31-$n_oggi;
        if ((date("Y")+1)%4){
                        $giorni=$giorni+1;
                }
    }
    else{
    //ciclo che calcola i giorni nei mesi mancanti
        for ($i=$mese;$i<=12;$i++){
            if ($i==4 || $i==6  || $i==9  || $i==11){
                $giorni=$giorni+30;
            }
            else{
                if ($i==2){
                    $giorni=$giorni+28;
                    if ((date("Y")+2)%4)
                        $giorni=$giorni+1;
                }
                else{
                    $giorni=$giorni+31;
                }
            }
        }//fine ciclo
        $giorni=$giorni-6-$n_oggi;
    }
    //echo "<br>somma mesi $giorni<br>";
    echo "giorni mancanti a Natale :<b> $giorni</b>";

    echo "<hr>";

    $n_num_richiesti=7;
    $numero=42;
    $cont_pari=0;
    if ($numero%2){ //rende pari i numeri dispari
        $numero=$numero+1;
    }
    else{
        echo "È già pari<br>";
    }
    for($i=0;$i<($n_num_richiesti);$i++){// stampa i numeri sommando al numero iniziale la tabellina del due($i*2)
        $num=$numero+($i*2);
        echo "$num<br>";
        $cont_pari=$cont_pari+1;
        if(!($cont_pari%2))
            echo "<br>";
    }

    echo "<hr>";

    $n_num_richiesti=7;
    $numero=42;
    $cont_pari=0;
    if ($numero%2){ //rende pari i numeri dispari
        $numero=$numero+1;
    }
    else{
        echo "È già pari<br>";
    }
    $i=0;
    while ($i<$n_num_richiesti){// stampa i numeri sommando al numero iniziale la tabellina del due($i*2)
        $num=$numero+($i*2);
        echo "$num<br>";
        $i++;
        $cont_pari=$cont_pari+1;
        if(!($cont_pari%2))
            echo "<br>";
    }


?>
</body>
</html>