<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php  //libreria
    function print_r_intestato($array){
        echo "<pre>";
        print_r ($array);
        echo "</pre>";
    }
    function br(){
        echo "<br>";
    }
    function stampa_array_2d($partecipanti){
        foreach($partecipanti as $key=>$value){
            foreach($partecipanti[$key] as $chiave=>$valore){
                echo $partecipanti[$key][$chiave];
                br();
            }
        }
    }
    
    ?>
    
    
    
    <?php
    $partecipanti=array(
        array('Primini Primo', 'Secondini Secondo'),
        array('Giannini Gianni', 'Stefanini Stefano', 'Mariolini Maria','Giacomini Giacomo'),
        array('Violetti Viola','Gaietti Gaia')
    );
    $corsi = array('Autocad','Informatica','Inglese');
    
    //print_r_intestato($partecipanti);
    //print_r_intestato($corsi);
    echo "<h3>punto 1</h3>";
    foreach ($corsi as $key=>$value){
        if($key%2){
            $sfondo="lightgray";
        }
        else{
            $sfondo="cadetblue";
        }
        echo "<div style=\"background-color:$sfondo\">$value</div>";
    }
    echo "<hr><h3>punto 2</h3>";
    stampa_array_2d($partecipanti);
    ?>

</body>
</html>