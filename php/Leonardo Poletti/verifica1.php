<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
    $start=10;
    $repeat=15;
    $numero=$start;
    $m=date("m");
    $d=date("d");
    //prima soluzione
    // for($i=1;$i<=$repeat;$i++){//ripete $repeat volte
    //     $numero--;
    // }

    //seconda soluzione
    $pari=0;
    $cont10=0;
    $numero_ripetizioni=0;
    for($i=$start;$i>=($start-$repeat) && $i>=0;$i--){
        echo "<div style=\"background-color:".(($i%2)?"#ddd":"#f6f6f6")."\">";
        echo $i-1;
        if($i==$d){
            echo "<strong>";
        }
        if($i==$m){
            echo "<i>";
        }
        if($i==$d)
            $pari++;
        echo "<font color='#999'> $i </font>";
        if($i==$d){
            echo "</strong>";
        }
        if($i==$m){
            echo "</i>";
        }
        if(!(($i+1)%10)){
            $cont10++;
        }
        echo ($i+1)." ";
        echo "<font color='red'>".($i*2)."</font>";
        echo "</div>";
        $numero_ripetizioni++;
        if ($i==date("y")){
            break;
        }
    }
    echo "<hr>";
    echo "\$start='$start' <br>\$repeat='$repeat'<br>";
    echo "\$i ".($i+1)."<br>";
    echo "multipli $cont10<br>";
    echo "ripetizioni $numero_ripetizioni<br>";
?>
</body>
</html>