<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Leonardo Poletti</title>
    <style>
    tr:nth-child(odd){
        background-color:lightgray;
    }
    table{
        border:1px solid black;
    }
    td{
        border:1px solid black;
    }
    </style>
</head>
<body>
    <?php  //libreria funzioni
    function intestazione($testo){
        echo "<h3>$testo</h3>";
    }

    function hr(){
        echo "<hr>";
    }
    function br(){
        echo "<br>";
    }
    function print_r_intestato($array){
        echo "<pre>";
        print_r ($array);
        echo "</pre>";
    }

    function genera_colore_hex(){
        $a=dechex(rand(0,255));
        // switch($a){ //non va 
        //     case 1:
        //     case 2:
        //     case 3:
        //     case 4:
        //     case 5:
        //     case 6:
        //     case 7:
        //     case 8:
        //     case 9:
        //     case 10:
        //     case ('a'):
        //     case ('b'):
        //     case ('c'):
        //     case ('d'):
        //     case ('e'):
        //     case ('f'):
        //     $a='0'.$a;
        //     break;
        // }
        return $a;
    }
    ?>
    
    <?php
    
    $colori=array(
        array('rgb'=>"0,0,0", 'data'=>"2018-01-01"),
        array('rgb'=>"255,255,255", 'data'=>"2018-05-21"),
        array('rgb'=>"255,0,0", 'data'=>"2018-12-07"),
        array('rgb'=>"255,255,255", 'data'=>"2018-12-07"),
    );
    //print_r_intestato($colori);

    intestazione("Punto 1");
    $a=genera_colore_hex();
    $b=genera_colore_hex();
    $c=genera_colore_hex();
    $colore_esa=("#".$a.$b.$c);
    echo "<div style=\"background-color:$colore_esa\">$colore_esa</div>";


    hr();
    intestazione("Punto 2. metto il print_r per far vedere direttamente il risultato");
    $rgbcod=hexdec($a).','.hexdec($b).','.hexdec($c);
    $colori[]=array('rgb'=>$rgbcod, 'data'=>date("Y-m-d"));
    print_r_intestato($colori);
   
    


    hr();
    intestazione("Punto 3");
    $conto=0;
    foreach($colori as $key=>$value){
        if($colori[$key]['rgb']==$rgbcod){
            $conto++;
        }
    }
    
    echo "<div style=\"background-color:rgb(".$colori[4]['rgb'].")\">".$colori[4]['rgb']." appare $conto volta/e</div>";


    hr();
    intestazione("Punto 4");
    $contodata=0;
    $oggi=date("Y-m-d");
    foreach($colori as $key=>$value){
        if($colori[$key]['data']==$oggi){
            $contodata++;
        }
    }
    echo "<div style=\"background-color:rgb(".$colori[4]['rgb'].")\">".$colori[4]['rgb']." appare $conto volta/e</div>";
    br();
    echo "sono stati aggiunti $contodata colori il $oggi</div>";

    hr();
    intestazione("Punto 5");
    foreach($colori as $key=>$value){//conta le apparizioni dei singoli colori
        $colori[$key]['numero_apparizioni']=1;
    }
    //print_r_intestato($colori);
    foreach($colori as $key=>$value){
        $verifica=true;
        for ($i=0; $i<$key;$i++){
            if($colori[$key]['rgb']==$colori[$i]['rgb']){
                $colori[$i]['numero_apparizioni']++;
                $verifica=false;
            }
            if($verifica==false){
                
                break;
            }
        }
    }
    $apparizione_alto=0;
    echo "<table>";//parte che stampa la tabella
    foreach($colori as $key=>$value){
        $verifica=true;
        for ($i=0; $i<$key;$i++){
            if($colori[$key]['rgb']==$colori[$i]['rgb']){
                $verifica=false;
                break;
            }
        }
        if($verifica==true){
            echo "<tr>";
            echo "<td>".$colori[$i]['rgb']."</td>";
            echo "<td>".$colori[$i]['numero_apparizioni']."</td>";
            echo "</tr>";
            if($colori[$i]['numero_apparizioni']>$apparizione_alto){
                $apparizione_alto=$colori[$i]['numero_apparizioni'];
                $piu_presente=$colori[$i]['rgb'];
            }
        }
    }
    echo "<table>";
    echo "il valore rgb presente più volte è $piu_presente con le sue $apparizione_alto apparizioni";
    ?>

</body>
</html>