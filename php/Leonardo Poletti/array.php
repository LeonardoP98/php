<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Array</title>
</head>
<body>
    <?php
    $a=array(1,2,5,40,"stringa","ultimo elemento");
    echo $a[0]."<br>";
    echo $a[1]."<br>";
    echo $a[2]."<br>";
    echo $a[3]."<br>";
    echo $a[4]."<br>";
    echo $a[5]."<br>";
    echo "<hr>";
    $a[]="valore aggiunto";
    $numero_elementi=count($a);
    for ($i=0;$i<$numero_elementi/*o count($a) */;$i++){
        echo ($i+1).")";
        echo $a[$i]."<br>";
    }
    echo "<hr>";

    $n_elementi=23;
    for ($i=0;$i<$n_elementi;$i++){
        $voti[]=100-$i;
    }
    for ($i=0;$i<count($voti);$i++){
        echo ($i+1).")";
        echo $voti[$i]."<br>";
    }

    echo "<hr>";

    $voti=array();
    $n_elementi=23;
    $somma=0;
    $cont_prom=0;
    for ($i=0;$i<$n_elementi;$i++){
        $b=rand(0, 100);
        $voti[]=$b;
    }
    for ($i=0;$i<count($voti);$i++){
        echo ($i+1).")";
        echo $voti[$i]."<br>";
    }
    for($i=0;$i<(count($voti));$i++){
        $somma=$somma+$voti[$i];
    }
    $media=$somma/(count($voti));
    echo "la media è: ".$media;
    echo "<hr>";
    $alto=$voti[0];
    $preso_alto=array();
    $c=0;
    for ($i=0;$i<count($voti);$i++){
        if($voti[$i]>=60){
            $cont_prom=$cont_prom+1;
        }
        if($voti[$i] > $alto) {
            $alto = $voti[$i];
        }
    }
    for ($i=0;$i<count($voti);$i++){
        if($voti[$i]==$alto) {
            $preso_alto[]=$i;
            $c=$c+1;
        }
    }
    echo "numero promossi: $cont_prom<br>";
    echo "numero più alto: $alto";
    echo "<br>lo ha preso $c individui <br> ";
    echo "ed è stato preso da:<ul>";
    foreach($preso_alto as $valore){
        echo "<li>".($valore+1)."</li>";
    }
    echo "</ul>";
    
    echo "<hr>";

    $voti=array();
    $n_elementi=23;
    $somma=0;
    $cont_prom=0;
    for ($i=0;$i<$n_elementi;$i++){
        $b=rand(0, 100);
        $voti[]=$b;
    }
    for ($i=0;$i<count($voti);$i++){
        echo ($i+1).")";
        echo $voti[$i]."<br>";
    }
    for($i=0;$i<(count($voti));$i++){
        $somma=$somma+$voti[$i];
    }
    $media=$somma/(count($voti));
    echo "la media è: ".$media;

    echo "<hr>";

    $alto=$voti[0];
    $preso_alto2=array();
    for ($i=0;$i<count($voti);$i++){
        if($voti[$i]>=60){
            $cont_prom=$cont_prom+1;
        }
        if($voti[$i] > $alto) {
            $alto = $voti[$i];
            //nuov voto + alto => cancello il contenuto
            $preso_alto2=array();
        }
        if ($voti==$alto){
            $preso_alto2[]=$i;
        }
    }
    echo "numero promossi: $cont_prom<br>";
    echo "numero più alto: $alto";
    echo "<br>ed è stato preso da:<ul>";
    foreach($preso_alto2 as $valore){
        echo "<li>".($valore+1)."</li>";
    }
    echo "</ul>";
    for($i=0;$i<count($preso_alto2);$i++){
        echo ($i+1)."<br>";
    }

    echo "<hr>";

    $prova=array(20,30);
    $prova[]=27;
    $prova[1]="stringa";
    $prova[3]=15;
    $prova[100]=1;
    foreach($prova as $valore){
        echo $valore."<br>";
    }
    echo "<hr>";
    foreach($prova as $key=>$valore){
        echo $valore."<br>";
    }
    echo "<hr>";
    foreach($prova as $key=>$valore){
        echo $prova[$key]."<br>";
    }
    echo "<hr>";

    $b=array('pino'=>1,'gino'=>2, 5=>3);
    $b[]="nuovo";
    foreach($b as $valore){
        echo $valore."<br>";
    }
    $posti=array();
    $n_posti_richiesti=10;
    $va_bene=true;
    $doppio=0;
    for ($i=0;$i<$n_posti_richiesti;$i++){
        $b=rand(30,100);
        for($j=0; $j<=($i-1);$j++){
            if($b==$posti[$j]){
                $doppio++;
                $va_bene=false;
                break;
            }
        }
        if($va_bene){ 
            $posti[$i]=$b;
        }
        else{
            $i--;
        }

    }
    for($i=0;$i<count($posti);$i++){
        echo $posti[$i]."<br>";
    }
    echo "sono stati fatto $doppio cicli j";
    ?>
</body>
</html>